const redis = require("redis");
const client = redis.createClient();

client.on("error", function(error) {
  console.error(error);
});

client.set('key', "My value from redis");

const Srf = require('drachtio-srf');
const srf = new Srf();

srf.connect({
  host: '127.0.0.1',
  port: 9022,
  secret: 'cymru'
});

srf.on('connect', (err, hostport) => {
  console.log(`connected to a drachtio server listening on: ${hostport}`);
});

var reply_ans;

srf.options((req, res) => {

 client.get('key', function(err,reply) { 
     reply_ans = reply; 
     console.log('1:'+reply);
    }
 );

 console.log('2:'+reply_ans);

 res.send(200, {
    headers: {
      'X-Freelancer-header': reply_ans
    }
  });
});

srf.invite((req, res) => {
  res.send(486, 'Busy-Vusy', {
    headers: {
      'X-Custom-Header': 'Test header'
    }
  });
});